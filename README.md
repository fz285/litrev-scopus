# litrev_scopus

This is an extended version of https://gitlab.developers.cam.ac.uk/yc366/litrev_biomarkers_for_targeted_med_or_icpi_induced_ild. 

## New Features
Scopus Search: Easily search and retrieve published papers using various criteria, such as author names, keywords, publication years, and more.

Customisable Queries: Tailor your search queries to match specific research interests and requirements.

Data Extraction: Extract essential information from the retrieved papers, including titles, abstracts, author names, publication dates, and more.

Filtering: Filter the papers based on your inclusion and exclusion criteria. 

Data Export: Export the retrieved data to various formats, such as JSON, CSV, or a database, for further analysis or integration with other tools.



## Prerequisites

List the prerequisites that users need to have installed on their system before they can use your project. Include links or instructions for installation when necessary. For example:

- [Docker](https://www.docker.com/get-started)

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.developers.cam.ac.uk/fz285/litrev-scopus.git

2. Start the container:

   ```bash
   docker-compose up

3. Click on the link from the terminal

## Usage

   There are two files that you can use to access papers based on your keywords and filter the results according to your criteria.

    1. **Scopus_search.ipynb**: This file allows you to retrieve all published papers and export the results to a CSV file.

    2. **Scopus_filter.ipynb**: This file enables you to filter the results based on your inclusion and exclusion criteria.


