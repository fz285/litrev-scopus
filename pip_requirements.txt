# anything that cannot be installed by conda
# need to be installed via pip.


# The package below is an internal package used to read nested feature tables from Compound Discoverer
# It is an example of OML internal python packages. All OML internal packages should be installed via pip (so add them to this list).
# Uncomment the line below if you want to install the package owl_gcms_feature_io
#owl_gcms_feature_io  


# Pleas list packages you need to install via pip

# package for pandas to download data from BigQuery
# pandas-gbq==0.13.2 

# package required for pandas to read excel files
# xlrd >= 1.0.0 

# package to allow python to run R code, requires r-base to be listed in the environment.yml file.
# rpy2==3.3.2 