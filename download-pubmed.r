library("remotes")
library("devtools")
library("RISmed")

# Search terms in query are case insensitive
SearchPubMed<-function(query, maxdate=2021, mindate=2016, retmax=100){
    
    search_query<-EUtilsSummary(query, retmax=retmax, maxdate=maxdate, mindate=mindate)
    records<-EUtilsGet(search_query)
    
    #concatenate authors:
    authors<-vector()
    author1<-vector()
    for(n in 1:length(records@Author)){
        if (is.data.frame(records@Author[[n]])){
            tmp<-paste0(records@Author[[n]]$LastName,", ",records@Author[[n]]$Initials)
            tmp2<-paste(tmp, collapse="; ")
            tmp_author1 <- records@Author[[n]]$LastName[1]
        }else{
            tmp2 <- NA
            tmp_author1 <- NA
        }
        authors<-append(authors,tmp2)
        print(tmp2)
        author1<-append(author1, tmp_author1)
    }
    
    # we need to add additional fields
    pubmed_data <- data.frame(
        'authors'=authors, 
        'title'=ArticleTitle(records),
        'publication_type'=PublicationType(records),
        'abstract'=AbstractText(records),
        'journal'=MedlineTA(records), 
        'pages'=MedlinePgn(records),
        'volume'=Volume(records),
        'issue'=Issue(records),
        'year'=YearPubmed(records),
        'pmid'=PMID(records), 
        'doi'=DOI(records),
        'url1'=paste0("https://www.ncbi.nlm.nih.gov/pubmed/",PMID(records)),
        'author1'=author1
    )
    
    return(pubmed_data)
}

SearchSavePubMed <- function(query, maxdate=2021, mindate=2016, save_to=NA, retmax=100){
    res =SearchPubMed(query, maxdate=maxdate, mindate=mindate, retmax=retmax)
    write.csv(res, save_to, row.names = FALSE, fileEncoding = "UTF-8")
    return(res)
}

