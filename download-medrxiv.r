######### MEDRXIV ###################
#install.packages("medrxivr")
library(medrxivr)
 
SearchMedrxiv<-function(query, maxdate="2023-02-07", mindate="2015-01-01"){
       
    if (!is.list(query)){
        stop("Query needs to be a list of character vectors, each vector represents an OR combination, vectors are combined via AND.")
    }else{
        
        # preprint_data <- mx_api_content(server = "medrxiv")
        preprint_data <- mx_api_content(server = "medrxiv", from_date = "2015-01-01", 
                               to_date = "2023-02-09")
        # Import the medrxiv database
        # preprint_data <- mx_snapshot()
        results <- mx_search(
            data = preprint_data,
            query = query, 
            fields = c("title", "abstract"), 
            from_date=mindate, 
            to_date=maxdate, 
            auto_caps=TRUE,
            report = TRUE
        )
        print(results)
    }

    return(results)
}

SearchSaveMedrxiv<-function(query, maxdate="2023-02-07", mindate="2015-01-01", save_to=NA){
    res =SearchMedrxiv(query, maxdate=maxdate, mindate=mindate)
    write.csv(res, save_to, row.names = FALSE, fileEncoding = "UTF-8")
    return(res)
}