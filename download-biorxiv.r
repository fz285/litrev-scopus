######### BIORXIV ###################
#install.packages("medrxivr")
library(medrxivr)
getBiorxiv<-function(){
   preprint_data <- mx_api_content(server = "biorxiv")
   return(preprint_data) 
    
}


SearchBiorxiv<-function(query, maxdate="2021-11-17", mindate="2020-11-17", pd){
    if (!is.list(query)){
        stop("Query needs to be a list of character vectors, each vector represents an OR combination, vectors are combined via AND.")
    }else{
        # Import the medrxiv database
        # preprint_data <- mx_api_content(server = "biorxiv")
        
        # Perform an advanced search
        results <- mx_search(
            data = pd,
            query = query, 
            fields = c("title", "abstract"), 
            from_date=mindate, 
            to_date=maxdate, 
            auto_caps=TRUE,
            report = TRUE
        )
    }
    return(results)
}

SearchSaveBiorxiv<-function(query, maxdate="2021-11-17", mindate="2020-11-17", save_to=NA, pd){
    res =SearchBiorxiv(query, maxdate=maxdate, mindate=mindate, pd)
    write.csv(res, save_to, row.names = FALSE, fileEncoding = "UTF-8")
    return(res)
}